export const feedsActionTypes = {
  MY_FEEDS_REQUEST: 'MY_FEEDS_REQUEST',
  MY_FEEDS_RESPONSE: 'MY_FEEDS_RESPONSE',
}

export const feedsActions = {
  loadMyFeeds: function () {
    return {
      type: feedsActionTypes.MY_FEEDS_REQUEST
    }
  },
}

const initState = {
  myFeedsIsFetching: false,
  myFeeds: [],
}

export function feedsReducer(state = initState, action) {
  switch (action.type) {
    case feedsActionTypes.MY_FEEDS_REQUEST:
      return {
        ...state,
        myFeedsIsFetching: true,
        myFeeds: [],
      }
    case feedsActionTypes.MY_FEEDS_RESPONSE:
      return {
        ...state,
        myFeedsIsFetching: false,
        myFeeds: action.data,
      }
    default:
      return state
  }
}
