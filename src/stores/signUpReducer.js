export const signUpActionTypes = {
  SIGN_UP_REQUEST: 'SIGN_UP_REQUEST',
  SIGN_UP_RESPONSE: 'SIGN_UP_RESPONSE',
}

export const signUpActions = {
  signUpRequest: function(data, formikBag) {
    return {type: signUpActionTypes.SIGN_UP_REQUEST, data, formikBag}
  },
}

const initState = {
  signUpIsFetching: true,
}

export function signUpReducer (state = initState, action) {
  switch (action.type) {
    case signUpActionTypes.SIGN_UP_REQUEST:
      return {
        ...state,
        signUpIsFetching: true,
      }
    case signUpActionTypes.SIGN_UP_RESPONSE:
      return {
        ...state,
        signUpIsFetching: false,
      }
    default:
      return state
  }
}
