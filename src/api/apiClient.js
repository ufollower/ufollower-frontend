import axios from 'axios'
import { getToken } from '../helpers'

const reactAppServer = process.env.REACT_APP_SERVER
const baseURL = (reactAppServer ? reactAppServer : 'http://localhost:8000/')
const apiClient = axios.create({
  baseURL,
})

export async function apiPostSignIn(data) {
  const url = `/api/accounts/sign-in/`
  const response = await apiClient.post(url, data)
  return response.data
}

export async function apiPostSignUp(data) {
  const url = `/api/accounts/sign-up/`
  const response = await apiClient.post(url, data)
  return response.data
}

export async function apiPostSignOut() {
  const url = `/api/accounts/sign-out/`
  const response = await apiClient.post(url, {},)
  return response.data
}

export async function apiGetProfile() {
  const url = `/api/accounts/profile/`
  const response = await apiClient.get(url,)
  return response.data
}

export async function apiPostPasswordChange(data) {
  const url = `/api/accounts/password-change/`
  const response = await apiClient.post(url, data)
  return response.data
}

export async function apiGetMyFeeds() {
  const url = `/api/feeds/my/`
  const response = await apiClient.get(url)
  return response.data
}

export async function apiGetMyEntries() {
  const url = `/api/entries/my/`
  const response = await apiClient.get(url)
  return response.data
}

export async function apiGetSavedEntries() {
  const url = `/api/entries/saved/`
  const response = await apiClient.get(url)
  return response.data
}

export async function apiGetFeedEntries(feedId) {
  const url = `/api/entries/?feed_id=${feedId}`
  const response = await apiClient.get(url)
  return response.data
}

apiClient.interceptors.request.use(function (config) {
  const token = getToken()
  if (token) {
    config.headers.Authorization = `Token ${token}`
  }

  return config
})
