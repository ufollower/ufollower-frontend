import React from 'react'
import { withFormik, Form } from 'formik'
import Button from '@material-ui/core/Button'
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';

import * as yup from 'yup'

const SignInForm = ({ signInFetching, handleSubmit, handleChange, handleBlur, values, touched, isValid, errors, status }) => {
  return (
    <Form className="sign-in__form">
      <h1>Sign In</h1>
      <FormControl error={!!errors.username} fullWidth>
        <InputLabel htmlFor="username">Username</InputLabel>
        <Input
          id="username"
          value={values.username}
          onChange={handleChange}
          aria-describedby="username-text"
        />
        <FormHelperText id="username-text">
          {errors.username}
        </FormHelperText>
      </FormControl>
      <FormControl error={!!errors.password} fullWidth>
        <InputLabel htmlFor="password">Password</InputLabel>
        <Input
          type="password"
          id="password"
          value={values.password}
          onChange={handleChange}
          aria-describedby="password-text"
        />
        <FormHelperText id="password-text">
          {errors.password}
        </FormHelperText>
      </FormControl>
      <Button
        type="submit"
        variant="contained"
        color="primary"
        disabled={signInFetching}
        fullWidth
      >
        Sign In
      </Button>
      {status && status.error && <div className="text-danger">{status.error}</div>}
    </Form>
  )
}

export default withFormik({
  mapPropsToValues: () => ({ username: '', password: '' }),

  validationSchema: yup.object().shape({
    username: yup.string().required(),
    password: yup.string().required(),
  }),

  handleSubmit: (values, formikBag) => {
    formikBag.props.signInRequest(values, formikBag)
  },

})(SignInForm)

