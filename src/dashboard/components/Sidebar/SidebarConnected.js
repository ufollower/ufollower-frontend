import { connect } from 'react-redux'

import { authActions } from '../../../stores/authReducer'
import { feedsActions } from '../../../stores/feedsReducer'

import Sidebar from './Sidebar'


const mapStateToProps = (state) => {
  return {
    myFeeds: state.feeds.myFeeds,
    myFeedsIsFetching: state.feeds.myFeedsIsFetching,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signOutRequest() {
      return dispatch(authActions.signOut())
    },
    loadMyFeedsRequest() {
      return dispatch(feedsActions.loadMyFeeds())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar)
