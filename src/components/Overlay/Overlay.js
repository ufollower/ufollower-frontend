import React from 'react'

import './Overlay.scss'


export default (props) => {
  return (
    <div className={'overlay'}>
      {props.children}
    </div>
  )
}
