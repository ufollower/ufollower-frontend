import { connect } from 'react-redux'

import { signUpActions } from '../../../stores/signUpReducer'

import SignInForm from './SignUpForm'

const mapStateToProps = (state) => {
  return {
    signUpFetching: state.auth.signUpIsFetching,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signUpRequest(values, formikBag) {
      return dispatch(signUpActions.signUpRequest({username: values.username, password: values.password}, formikBag))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignInForm)

