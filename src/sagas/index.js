import { all } from 'redux-saga/effects'
import { watchAuthAsync } from './authSaga'
import { watchSignUpAsync } from './signUpSaga'
import { watchFeedsAsync } from './feedsSaga'
import { watchEntriesAsync } from './entriesSaga'

export default function* rootSaga() {
  yield all([
    watchAuthAsync(),
    watchSignUpAsync(),
    watchFeedsAsync(),
    watchEntriesAsync(),
  ])
}
