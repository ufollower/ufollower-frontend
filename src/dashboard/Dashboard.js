import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import Routes from './routes'
import Sidebar from './components/Sidebar'
import Home from './pages/Home'
import Saved from './pages/Saved'
import PasswordChange from './pages/PasswordChange'


const Dashboard = (props) => {
  return (
    <React.Fragment>
      <Sidebar/>
      <Switch>
        <Route path={Routes.index} exact component={Home} />
        <Route path={Routes.saved} exact component={Saved} />
        <Route path={Routes.passwordChange} exact component={PasswordChange} />
        <Redirect to={Routes.index} />
      </Switch>
    </React.Fragment>
  )
}

export default Dashboard