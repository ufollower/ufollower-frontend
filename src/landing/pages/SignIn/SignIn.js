import React from 'react'

import SignInFormConnected from './SignInFormConnected'
import './SignIn.scss'


function SignIn(props) {
  return (
    <main className="sign-in">
      <SignInFormConnected/>
    </main>
  )
}

export default SignIn