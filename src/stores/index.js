import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'

import { authReducer } from './authReducer'
import { signUpReducer } from './signUpReducer'
import { feedsReducer } from './feedsReducer'
import { entriesReducer } from './entriesReducer'

export default (history) => combineReducers({
  router: connectRouter(history),
  auth: authReducer,
  signUp: signUpReducer,
  feeds: feedsReducer,
  entries: entriesReducer,
})
