export default {
  index: '/',
  saved: '/saved',
  passwordChange: '/password-change',
}
