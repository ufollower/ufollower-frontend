import { call, put, takeEvery } from 'redux-saga/effects'

import { authActionTypes } from '../stores/authReducer'
import { apiGetProfile } from '../api/apiClient'
import { apiPostSignIn  } from '../api/apiClient'
import { apiPostSignOut } from '../api/apiClient'
import { apiPostPasswordChange } from '../api/apiClient'
import { setToken } from '../helpers'
import { removeToken } from '../helpers'
import { getToken } from '../helpers'


function* profileWorker() {
  try {
    const token = yield call(getToken)
    if (!token) {
      yield put({type: authActionTypes.PROFILE_FAILURE})
    } else {
      const data = yield call(apiGetProfile)
      yield put({type: authActionTypes.PROFILE_SUCCESS, data: data})
    }
  } catch (e) {
    if ( e.response && e.response.status === 401) {
      yield call(removeToken)
    }
    yield put({type: authActionTypes.PROFILE_FAILURE})
  }
}

function* signInWorker(action) {
  try {
    const data = yield call(apiPostSignIn, action.data)
    yield call(setToken, data.token)
    yield put({type: authActionTypes.PROFILE_REQUEST})
  } catch (e) {
    if (e.response && e.response.status < 500) {
      if (e.response.data.detail) {
        action.formikBag.setStatus({
          error: e.response.data.detail
        })
      } else {
        action.formikBag.setErrors(e.response.data)
      }
    } else {
      action.formikBag.setStatus({
        error: 'Something went wrong. Try again later.'
      })
    }
  } finally {
    yield put({type: authActionTypes.SIGN_IN_RESPONSE})
  }
}

function* signOutWorker(action) {
  try {
    yield call(apiPostSignOut)
    yield call(removeToken)
    yield put({type: authActionTypes.PROFILE_FAILURE})
  } catch (e) {

  } finally {
    yield put({type: authActionTypes.SIGN_OUT_RESPONSE})
  }
}

function* passwordChangeWorker(action) {
  try {
    yield call(apiPostPasswordChange, action.data)
    yield call(action.formikBag.resetForm)
    yield call(action.formikBag.setStatus, {success: 'Password changed!'})
  } catch (e) {
    if (e.response && e.response.status < 500) {
      const data = e.response.data
      if (data.detail) {
        action.formikBag.setStatus({error: data.detail})
      } else {
        data.old_password && action.formikBag.setFieldError('oldPassword', data.old_password[0])
        data.new_password && action.formikBag.setFieldError('newPassword', data.new_password[0])
      }
    } else {
      action.formikBag.setStatus({
        error: 'Something went wrong. Try again later.'
      })
    }
  } finally {
    yield put({type: authActionTypes.PASSWORD_CHANGE_RESPONSE})
  }
}

export function* watchAuthAsync() {
  yield takeEvery(authActionTypes.PROFILE_REQUEST, profileWorker)
  yield takeEvery(authActionTypes.SIGN_IN_REQUEST, signInWorker)
  yield takeEvery(authActionTypes.SIGN_OUT_REQUEST, signOutWorker)
  yield takeEvery(authActionTypes.PASSWORD_CHANGE_REQUEST, passwordChangeWorker)
}
