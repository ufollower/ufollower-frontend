import { connect } from 'react-redux'

import { authActions } from '../../../stores/authReducer'

import PasswordChangeForm from './PasswordChangeForm'

const mapStateToProps = (state) => {
  return {
    passwordChangeFetching: state.auth.passwordChangeIsFetching,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    passwordChangeRequest(values, formikBag) {
      return dispatch(authActions.changePassword(
        {'old_password': values.oldPassword, 'new_password': values.newPassword}, formikBag
      ))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PasswordChangeForm)

