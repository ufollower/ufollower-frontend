import { call, put, takeEvery } from 'redux-saga/effects'

import { entriesActionTypes } from '../stores/entriesReducer'
import { apiGetMyEntries } from '../api/apiClient'
import { apiGetSavedEntries } from '../api/apiClient'
import { apiGetFeedEntries } from '../api/apiClient'


function* myEntriesWorker() {
  try {
    const data = yield call(apiGetMyEntries)
    yield put({type: entriesActionTypes.MY_ENTRIES_RESPONSE, data: data})
  } catch (e) {
    yield put({type: entriesActionTypes.MY_ENTRIES_RESPONSE, data: []})
  }
}

function* savedEntriesWorker() {
  try {
    const data = yield call(apiGetSavedEntries)
    yield put({type: entriesActionTypes.SAVED_ENTRIES_RESPONSE, data: data})
  } catch (e) {
    yield put({type: entriesActionTypes.SAVED_ENTRIES_RESPONSE, data: []})
  }
}

function* feedEntriesWorker(action) {
  try {
    const data = yield call(apiGetFeedEntries, action.feedId)
    yield put({type: entriesActionTypes.FEED_ENTRIES_RESPONSE, data: data})
  } catch (e) {
    yield put({type: entriesActionTypes.FEED_ENTRIES_RESPONSE, data: []})
  }
}

export function* watchEntriesAsync() {
  yield takeEvery(entriesActionTypes.MY_ENTRIES_REQUEST, myEntriesWorker)
  yield takeEvery(entriesActionTypes.SAVED_ENTRIES_REQUEST, savedEntriesWorker)
  yield takeEvery(entriesActionTypes.FEED_ENTRIES_REQUEST, feedEntriesWorker)
}
