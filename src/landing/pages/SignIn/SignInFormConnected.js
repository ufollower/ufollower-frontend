import { connect } from 'react-redux'

import { authActions } from '../../../stores/authReducer'

import SignInForm from './SignInForm'

const mapStateToProps = (state) => {
  return {
    signInFetching: state.auth.signInIsFetching,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signInRequest(values, formikBag) {
      return dispatch(authActions.signIn(values, formikBag))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignInForm)
