import { connect } from 'react-redux'

import { entriesActions } from '../../../stores/entriesReducer'

import Home from './Home'


const mapStateToProps = (state) => {
  return {
    entries: state.entries.entries,
    myEntriesIsFetching: state.entries.myEntriesIsFetching,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadMyEntriesRequest() {
      return dispatch(entriesActions.loadMyEntries())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
