import React from 'react'
import { withFormik, Form } from 'formik'
import Button from '@material-ui/core/Button'
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import * as yup from 'yup'

const SignUpForm = ({ signUpFetching, handleSubmit, handleChange, handleBlur, values, touched, isValid, errors, status }) => {
  return (
    <Form className="sign-up__form">
      <h1>Sign Up</h1>
      <FormControl error={!!errors.username} fullWidth>
        <InputLabel htmlFor="username">Username</InputLabel>
        <Input
          id="username"
          value={values.username}
          onChange={handleChange}
          aria-describedby="username-text"
        />
        <FormHelperText id="username-text">
          {errors.username}
        </FormHelperText>
      </FormControl>
      <FormControl error={!!errors.password} fullWidth>
        <InputLabel htmlFor="password">Password</InputLabel>
        <Input
          type="password"
          id="password"
          value={values.password}
          onChange={handleChange}
          aria-describedby="password-text"
        />
        <FormHelperText id="password-text">
          {errors.password}
        </FormHelperText>
      </FormControl>
      <FormControl error={!!errors.passwordConfirm} fullWidth>
        <InputLabel htmlFor="passwordConfirm">Confirm Password</InputLabel>
        <Input
          type="password"
          id="passwordConfirm"
          value={values.passwordConfirm}
          onChange={handleChange}
          aria-describedby="password-confirm-text"
        />
        <FormHelperText id="password-confirm-text">
          {errors.passwordConfirm}
        </FormHelperText>
      </FormControl>
      <Button
        type="submit"
        variant="contained"
        color="primary"
        disabled={signUpFetching}
        fullWidth
      >
        Sign Up
      </Button>
      {status && status.error && <div className="text-danger">{status.error}</div>}
      {status && status.success && <div className="text-success">{status.success}</div>}
    </Form>
  )
}

export default withFormik({
  mapPropsToValues: () => ({ username: '', password: '', passwordConfirm: ''}),

  validationSchema: yup.object().shape({
    username: yup.string().required(),
    password: yup.string().required('This field is required'),
    passwordConfirm: yup.string().required('This field is required')
      .oneOf([yup.ref('password'), null], 'Passwords do not match')
  }),

  handleSubmit: (values, formikBag) => {
    formikBag.props.signUpRequest(values, formikBag)
  },

})(SignUpForm)

