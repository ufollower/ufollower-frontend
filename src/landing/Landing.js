import React, {Fragment} from 'react'

import { Switch, Route, Redirect } from 'react-router-dom'

import Routes from './routes'
import Header from './components/Header'
import Home from './pages/Home'
import SignIn from './pages/SignIn'
import SignUp from './pages/SignUp'

const Landing = (props) => {
  return (
    <Fragment>
      <Header/>
      <Switch>
        <Route path={Routes.index} exact component={Home} />
        <Route path={Routes.signIn} component={SignIn} />
        <Route path={Routes.signUp} component={SignUp} />
        <Redirect to={Routes.index} />
      </Switch>
    </Fragment>
  )
}

export default Landing
