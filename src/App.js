import React from 'react'
import { ConnectedRouter } from 'connected-react-router'
import CircularProgress from '@material-ui/core/CircularProgress'

import { history } from './index'
import Overlay from './components/Overlay'
import Landing from './landing/Landing'
import Dashboard from './dashboard/Dashboard'

import './App.scss';


class App extends React.Component {
  componentDidMount() {
    this.props.getProfile()
  }

  render() {
    return (
      <ConnectedRouter history={history}>
        <div className="app">
          {this.props.auth.profileIsFetching ?
            <Overlay>
              <CircularProgress />
            </Overlay>
            :
            this.props.auth.isAuthorized ?
              <Dashboard />
              :
              <Landing />
          }
        </div>
      </ConnectedRouter>
    );
  }
}

export default App;



