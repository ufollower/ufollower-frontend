import { connect } from 'react-redux'

import { authActions } from './stores/authReducer'
import App from './App'


const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getProfile() {
      return dispatch(authActions.getProfile())
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
