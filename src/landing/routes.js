export default {
  index: '/',
  signIn: '/sign-in',
  signUp: '/sign-up',
}
