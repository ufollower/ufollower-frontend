import React, { useState, useEffect } from 'react'
import cx from 'classnames'
import { NavLink } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import Divider from '@material-ui/core/Divider'
import IconButton from '@material-ui/core/IconButton'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Collapse from '@material-ui/core/Collapse'
import MenuIcon from '@material-ui/icons/Menu'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'
import SettingsApplicationsIcon from '@material-ui/icons/SettingsApplications'
import HomeIcon from '@material-ui/icons/Home'
import BookmarkIcon from '@material-ui/icons/Bookmark'
import AddIcon from '@material-ui/icons/Add'
import StorageIcon from '@material-ui/icons/Storage'

import Routes from '../../routes'

import './Sidebar.scss'

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(8) - 2,
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  nested: {
    paddingLeft: theme.spacing(5)
  }
}));


const Sidebar = (props) => {
  const classes = useStyles();

  const [isExpanded, toggleExpanded] = useState(false)

  useEffect(() => {props.loadMyFeedsRequest()}, [])

  return (
    <Drawer
      variant="permanent"
      className={cx(classes.drawer, {
        [classes.drawerOpen]: isExpanded,
        [classes.drawerClose]: !isExpanded,
      })}
      classes={{
        paper: cx({
          [classes.drawerOpen]: isExpanded,
          [classes.drawerClose]: !isExpanded,
        }),
      }}
      open={isExpanded}
    >
      <div className={classes.toolbar}>
        <IconButton onClick={() => toggleExpanded(!isExpanded)}>
          {isExpanded ? <ChevronLeftIcon /> : <MenuIcon />}
        </IconButton>
      </div>
      <Divider />
      <List>
        <ListItem component={NavLink} button to={Routes.index}>
          <ListItemIcon><HomeIcon/></ListItemIcon>
          <ListItemText primary={'Home'} />
        </ListItem>
        <ListItem component={NavLink} button to={Routes.saved}>
          <ListItemIcon><BookmarkIcon/></ListItemIcon>
          <ListItemText primary={'Saved'} />
        </ListItem>
        <ListItem button>
          <ListItemIcon><AddIcon/></ListItemIcon>
          <ListItemText primary={'Add feed'} />
        </ListItem>
        <ListItem button>
          <ListItemIcon><StorageIcon /></ListItemIcon>
          <ListItemText primary={'Feeds'} />
        </ListItem>
        <Collapse in={isExpanded} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {!props.myFeedsIsFetching && (
              props.myFeeds.map((row) => {
                return (
                  <ListItem button className={classes.nested} key={row.id}>
                    <ListItemText primary={row.title} />
                  </ListItem>
                )
              })
            )}
          </List>
        </Collapse>

      </List>
      <Divider />
      <List>
        <ListItem component={NavLink} button to={Routes.passwordChange}>
          <ListItemIcon><SettingsApplicationsIcon/></ListItemIcon>
          <ListItemText primary={'Change Password'} />
        </ListItem>
        <ListItem button onClick={() => {props.signOutRequest()}}>
          <ListItemIcon><ExitToAppIcon/></ListItemIcon>
          <ListItemText primary={'Sign Out'} />
        </ListItem>
      </List>
    </Drawer>
  )
}

export default Sidebar