export const entriesActionTypes = {
  MY_ENTRIES_REQUEST: 'MY_ENTRIES_REQUEST',
  MY_ENTRIES_RESPONSE: 'MY_ENTRIES_RESPONSE',
  SAVED_ENTRIES_REQUEST: 'SAVED_ENTRIES_REQUEST',
  SAVED_ENTRIES_RESPONSE: 'SAVED_ENTRIES_RESPONSE',
  FEED_ENTRIES_REQUEST: 'FEED_ENTRIES_REQUEST',
  FEED_ENTRIES_RESPONSE: 'FEED_ENTRIES_RESPONSE',
}

export const entriesActions = {
  loadMyEntries: function () {
    return {
      type: entriesActionTypes.MY_ENTRIES_REQUEST
    }
  },
  loadSavedEntries: function () {
    return {
      type: entriesActionTypes.SAVED_ENTRIES_REQUEST
    }
  },
  loadFeedEntries: function (feedId) {
    return {
      type: entriesActionTypes.FEED_ENTRIES_REQUEST, feedId
    }
  },
}

const initState = {
  myEntriesIsFetching: false,
  savedEntriesIsFetching: false,
  feedEntriesIsFetching: false,
  entries: [],
}

export function entriesReducer(state = initState, action) {
  switch (action.type) {
    case entriesActionTypes.MY_ENTRIES_REQUEST:
      return {
        ...state,
        myEntriesIsFetching: true,
        entries: [],
      }
    case entriesActionTypes.MY_ENTRIES_RESPONSE:
      return {
        ...state,
        myEntriesIsFetching: false,
        entries: action.data,
      }
    case entriesActionTypes.SAVED_ENTRIES_REQUEST:
      return {
        ...state,
        savedEntriesIsFetching: true,
        entries: [],
      }
    case entriesActionTypes.SAVED_ENTRIES_RESPONSE:
      return {
        ...state,
        savedEntriesIsFetching: false,
        entries: action.data,
      }
    case entriesActionTypes.FEED_ENTRIES_REQUEST:
      return {
        ...state,
        feedEntriesIsFetching: true,
        entries: [],
      }
    case entriesActionTypes.FEED_ENTRIES_RESPONSE:
      return {
        ...state,
        feedEntriesIsFetching: false,
        entries: action.data,
      }
    default:
      return state
  }
}
