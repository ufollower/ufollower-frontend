import React from 'react'

import PasswordChangeFormConnected from './PasswordChangeFormConnected'
import './PasswordChange.scss'


function PasswordChange(props) {
  return (
    <main className="password-change">
      <PasswordChangeFormConnected/>
    </main>
  )
}

export default PasswordChange