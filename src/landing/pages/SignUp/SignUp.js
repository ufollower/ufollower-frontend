import React from 'react'

import SignUpFormConnected from './SignUpFormConnected'
import './SignUp.scss'


function SignUp(props) {
  return (
    <main className="sign-up">
      <SignUpFormConnected/>
    </main>
  )
}

export default SignUp