import React from 'react'
import { withFormik, Form } from 'formik'
import Button from '@material-ui/core/Button'
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import * as yup from 'yup'

const PasswordChangeForm = (
    { passwordChangeFetching, handleSubmit, handleChange, handleBlur, values, touched, isValid, errors, status }
  ) => {
  return (
    <Form className="password-change__form">
      <h1>Change Password</h1>
      <FormControl error={!!errors.oldPassword} fullWidth>
        <InputLabel htmlFor="oldPassword">Old Password</InputLabel>
        <Input
          type="password"
          id="oldPassword"
          value={values.oldPassword}
          onChange={handleChange}
          aria-describedby="old-password-text"
        />
        <FormHelperText id="old-password-text">
          {errors.oldPassword}
        </FormHelperText>
      </FormControl>
      <FormControl error={!!errors.newPassword} fullWidth>
        <InputLabel htmlFor="newPassword">New Password</InputLabel>
        <Input
          type="password"
          id="newPassword"
          value={values.newPassword}
          onChange={handleChange}
          aria-describedby="new-password-text"
        />
        <FormHelperText id="new-password-text">
          {errors.newPassword}
        </FormHelperText>
      </FormControl>
      <FormControl error={!!errors.newPasswordConfirm} fullWidth>
        <InputLabel htmlFor="newPasswordConfirm">Confirm New Password</InputLabel>
        <Input
          type="password"
          id="newPasswordConfirm"
          value={values.newPasswordConfirm}
          onChange={handleChange}
          aria-describedby="new-password-confirm-text"
        />
        <FormHelperText id="new-password-confirm-text">
          {errors.newPasswordConfirm}
        </FormHelperText>
      </FormControl>
      <Button
        type="submit"
        variant="contained"
        color="primary"
        disabled={passwordChangeFetching}
        fullWidth
      >
        Change
      </Button>
      {status && status.error && <div className="text-danger">{status.error}</div>}
      {status && status.success && <div className="text-success">{status.success}</div>}
    </Form>
  )
}

export default withFormik({
  mapPropsToValues: () => ({ oldPassword: '', newPassword: '', newPasswordConfirm: '' }),

  validationSchema: yup.object().shape({
    oldPassword: yup.string().required('This field is required'),
    newPassword: yup.string().required('This field is required'),
    newPasswordConfirm: yup.string().required('This field is required')
      .oneOf([yup.ref('newPassword'), null], 'Passwords do not match')
  }),

  handleSubmit: (values, formikBag) => {
    formikBag.props.passwordChangeRequest(values, formikBag)
  },

})(PasswordChangeForm)

