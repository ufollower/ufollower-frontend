import { connect } from 'react-redux'

import { authActions } from '../../../stores/authReducer'
import Header from './Header'


const mapStateToProps = (state) => {
  return {
    isAuthorized: state.auth.isAuthorized,
    profile: state.auth.profile,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signOutRequest() {
      return dispatch(authActions.signOut())
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)
