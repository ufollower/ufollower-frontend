import React, { useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Container from '@material-ui/core/Container'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import Badge from '@material-ui/core/Badge'
import StarIcon from '@material-ui/icons/Star'
import BookmarkIcon from '@material-ui/icons/Bookmark'
import CommentIcon from '@material-ui/icons/Comment'

const useStyles = makeStyles(theme => ({
  home: {

  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
  },
  cardContent: {
    flexGrow: 1,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  comments: {
    marginLeft: 'auto',
  },
}));

function Home(props) {
  const classes = useStyles();

  useEffect(() => {
    props.loadMyEntriesRequest()
  }, [])

  return (
    <main className={classes.home}>
      <Container className={classes.cardGrid}>
        <Grid container spacing={4}>
          {props.entries.map(entry => (
            <Grid item key={entry.id} sm={12} md={6}>
              <Card className={classes.card}>
                <CardContent className={classes.cardContent}>
                  <Typography>
                    {entry.title}
                  </Typography>
                </CardContent>
                <CardActions disableSpacing>
                  <IconButton aria-label="star">
                    <StarIcon />
                  </IconButton>
                  <IconButton aria-label="save">
                    <BookmarkIcon />
                  </IconButton>
                  <IconButton
                    className={classes.comments}
                    aria-label="comments"
                  >
                    <Badge color="secondary" badgeContent={entry.comments} className={classes.margin}>
                      <CommentIcon />
                    </Badge>
                  </IconButton>
                </CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>

    </main>
  );
}

export default Home
