import { call, put, takeEvery } from 'redux-saga/effects'

import { signUpActionTypes } from '../stores/signUpReducer'
import { apiPostSignUp  } from '../api/apiClient'


export function* signUpWorker (action) {
  try {
    yield call(apiPostSignUp, action.data)
    yield call(action.formikBag.resetForm)
    yield call(action.formikBag.setStatus, {success: 'You can sign in into service now!'})
  } catch (e) {
    if (e.response && e.response.status < 500) {
      const data = e.response.data
      if (data.detail) {
        action.formikBag.setStatus({error: data.detail})
      } else {
        data.username && action.formikBag.setFieldError('username', data.username[0])
        data.password && action.formikBag.setFieldError('password', data.password[0])
      }
    } else {
      action.formikBag.setStatus({error: 'Something went wrong. Try again later.'})
    }
  } finally {
    yield put({type: signUpActionTypes.SIGN_UP_RESPONSE})
  }
}

export function* watchSignUpAsync() {
  yield takeEvery(signUpActionTypes.SIGN_UP_REQUEST, signUpWorker)
}
