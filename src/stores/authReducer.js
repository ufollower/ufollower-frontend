export const authActionTypes = {
  SIGN_IN_REQUEST: 'SIGN_IN_REQUEST',
  SIGN_IN_RESPONSE: 'SIGN_IN_RESPONSE',
  SIGN_OUT_REQUEST: 'SIGN_OUT_REQUEST',
  SIGN_OUT_RESPONSE: 'SIGN_OUT_RESPONSE',
  PASSWORD_CHANGE_REQUEST: 'PASSWORD_CHANGE_REQUEST',
  PASSWORD_CHANGE_RESPONSE: 'PASSWORD_CHANGE_RESPONSE',
  PROFILE_REQUEST: 'PROFILE_REQUEST',
  PROFILE_SUCCESS: 'PROFILE_SUCCESS',
  PROFILE_FAILURE: 'PROFILE_FAILURE',
}

export const authActions = {
  signIn: function (data, formikBag) {
    return {
      type: authActionTypes.SIGN_IN_REQUEST, data, formikBag
    }
  },
  signOut: function () {
    return {
      type: authActionTypes.SIGN_OUT_REQUEST,
    }
  },
  getProfile: function() {
    return {
      type: authActionTypes.PROFILE_REQUEST
    }
  },
  changePassword: function(data, formikBag) {
    return {
      type: authActionTypes.PASSWORD_CHANGE_REQUEST, data, formikBag
    }
  },
}

const initState = {
  signInIsFetching: false,
  signOutIsFetching: false,
  profileIsFetching: false,
  passwordChangeIsFetching: false,
  isAuthorized: false,
  profile: {
    username: '',
  },
}

export function authReducer(state = initState, action) {
  switch (action.type) {
    case authActionTypes.SIGN_IN_REQUEST:
      return {
        ...state,
        signInIsFetching: true,
      }
    case authActionTypes.SIGN_IN_RESPONSE:
      return {
        ...state,
        signInIsFetching: false,
      }
    case authActionTypes.SIGN_OUT_REQUEST:
      return {
        ...state,
        signOutIsFetching: true,
      }
    case authActionTypes.SIGN_OUT_RESPONSE:
      return {
        ...state,
        signOutIsFetching: false,
      }
    case authActionTypes.PASSWORD_CHANGE_REQUEST:
      return {
        ...state,
        passwordChangeIsFetching: true,
      }
    case authActionTypes.PASSWORD_CHANGE_RESPONSE:
      return {
        ...state,
        passwordChangeIsFetching: false,
      }
    case authActionTypes.PROFILE_REQUEST:
      return {
        ...state,
        profileIsFetching: true,
        isAuthorized: false,
      }
    case authActionTypes.PROFILE_SUCCESS:
      return {
        ...state,
        profileIsFetching: false,
        isAuthorized: true,
        profile: {
          ...action.data
        },
      }
    case authActionTypes.PROFILE_FAILURE:
      return {
        ...state,
        profileIsFetching: false,
        isAuthorized: false,
        profile: {
          ...initState.profile
        }
      }
    default:
      return state
  }
}
