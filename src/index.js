import React from 'react';
import ReactDOM from 'react-dom';
import { applyMiddleware, compose, createStore } from 'redux'
import { createBrowserHistory } from 'history'
import { routerMiddleware } from 'connected-react-router'
import { Provider } from 'react-redux'
import createSagaMiddleware from 'redux-saga'

import rootSaga from './sagas'
import reducers from './stores'
import AppConnected from './AppConnected';

import './index.scss';

const sagaMiddleware = createSagaMiddleware()
export const history = createBrowserHistory()
const connectedRouterMiddleware = routerMiddleware(history)

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(
  reducers(history),
  composeEnhancer(
    applyMiddleware(
      connectedRouterMiddleware,
      sagaMiddleware,
    ),
  ),
)

sagaMiddleware.run(rootSaga)

ReactDOM.render((
    <Provider store={store}>
      <AppConnected/>
    </Provider>
  ), document.getElementById('root')
)
