import { call, put, takeEvery } from 'redux-saga/effects'

import { feedsActionTypes } from '../stores/feedsReducer'
import { apiGetMyFeeds } from '../api/apiClient'


function* myFeedsWorker() {
  try {
    const data = yield call(apiGetMyFeeds)
    yield put({type: feedsActionTypes.MY_FEEDS_RESPONSE, data: data})
  } catch (e) {
    yield put({type: feedsActionTypes.MY_FEEDS_RESPONSE, data: []})
  }
}

export function* watchFeedsAsync() {
  yield takeEvery(feedsActionTypes.MY_FEEDS_REQUEST, myFeedsWorker)
}
