import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'

import { Link } from 'react-router-dom'

import Routes from '../../routes'

import './Header.scss'


function Header(props) {
  return (
    <AppBar className="header">
      <Toolbar>
        <Typography className="header__title">
          UFollower
        </Typography>
        <div>
          <Button component={Link} to={Routes.signIn}>Sign In</Button>
          <Button component={Link} to={Routes.signUp}>Sign Up</Button>
        </div>
      </Toolbar>
    </AppBar>
  )
}

export default Header
